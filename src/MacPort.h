/************************************************************************
 * IA-32/x64 Software Spectrometer
 * Copyright (C) 2019 Nelson Nunes
 *
 * Changes required to support compiling on MacOS
 ************************************************************************/

#include <unistd.h>
#include <sched.h>

void *aligned_alloc(size_t alignment, size_t size);
#define memalign(alignment,size) aligned_alloc(alignment,size)
#define pthread_yield() sched_yield()
