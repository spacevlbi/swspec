# ##### PLATFORM SELECT (edit manually...)

FLAG_HAVE_IPP2019 = true
FLAG_HAVE_PLPLOT = false

# To compile in 32 and 64-bit systems
OSTYPE := $(shell uname -s)
ifeq ($(OSTYPE), Darwin)
   FLAG_OSX = true
endif

ifeq ($(FLAG_HAVE_IPP2019), true)
   IPP_INCLUDE_PATH=/opt/intel/ipp/include/
   IPP_LEGACY_INCLUDE_PATH=/opt/intel/legacy/include/
   ifeq ($(FLAG_OSX), true)
      IPP_LIB_PATH=/opt/intel/ipp/lib/
      IPP_LEGACY_LIB_PATH=/opt/intel/legacy/lib/
      IPP_SHAREDLIB_PATH=/opt/intel/compilers_and_libraries/mac/lib/
   else
      IPP_LIB_PATH=/opt/intel/ipp/lib/intel64/
      IPP_LEGACY_LIB_PATH=/opt/intel/legacy/lib/intel64/
      IPP_SHAREDLIB_PATH=/opt/intel/compilers_and_libraries/linux/lib/intel64/
   endif
else
   IPP_INCLUDE_PATH=/opt/intel/ipp/6.1.5.061/em64t/include/
   IPP_LIB_PATH=/opt/intel/ipp/6.1.5.061/em64t/lib/
   IPP_SHAREDLIB_PATH=/opt/intel/ipp/6.1.5.061/em64t/sharedlib/
endif

CC = g++
CFLAGS = -g -O3 -Wall -pthread -DHAVE_MK5ACCESS=1 -I../mark5access/mark5access/

BASEFILES = swspectrometer.cpp TaskDispatcher.cpp FileSource.cpp FileSink.cpp TeeSink.cpp Buffer.cpp Helpers.cpp \
   DataSource.cpp DataSink.cpp VSIBSource.cpp IniParser.cpp LogFile.cpp IA-32/TaskCoreIPP.cpp IA-32/DataUnpackers.cpp IA-32/PhaseCal/PCal.cpp

ifeq ($(FLAG_OSX), true)
   BASEFILES := $(BASEFILES) MacPort.cpp
endif

ifeq ($(FLAG_HAVE_PLPLOT), true)
   CFLAGS := $(CFLAGS) -DHAVE_PLPLOT=1
   BASEFILES := $(BASEFILES) PlplotSink.cpp
endif

BASEOBJS = $(BASEFILES:.cpp=.o)
BUILD_NUMBER_FILE=build-number.txt

intel_CFLAGS = $(CFLAGS) -DINTEL_IPP=1 -I${IPP_INCLUDE_PATH} -I. -I./IA-32/
ifeq ($(FLAG_HAVE_IPP2019), true)
   intel_CFLAGS := $(intel_CFLAGS) -DHAVE_IPP2019=1 -I${IPP_LEGACY_INCLUDE_PATH}
endif

intel_LDFLAGS := -L../mark5access/mark5access/.libs/ -L${IPP_LIB_PATH} -Wl,-rpath,${IPP_LIB_PATH} -L${IPP_SHAREDLIB_PATH} -Wl,-rpath,${IPP_SHAREDLIB_PATH}
ifeq ($(FLAG_HAVE_IPP2019), true)
   intel_LDFLAGS := ${intel_LDFLAGS} -L${IPP_LEGACY_LIB_PATH} -Wl,-rpath,${IPP_LEGACY_LIB_PATH}
endif
ifeq ($(FLAG_OSX), true)
   intel_LDFLAGS := $(intel_LDFLAGS) -L/opt/X11/lib
endif                             

ifeq ($(FLAG_HAVE_IPP2019), true)
   intel_LDINCL = -liomp5 -lipps -lippvm -lippcore -lX11 -lm -pthread
   ifneq ($(FLAG_OSX), true)
      intel_LDINCL := -Wl,-Bdynamic $(intel_LDINCL)
   endif
else
   LBITS := $(shell getconf LONG_BIT)
   ifeq ($(LBITS),64)
      intel_LDINCL  =  -Wl,-Bdynamic -lguide -lippsem64t -lippvmem64t -lippcoreem64t -lX11 -lm -pthread
      intel_LDINCL_S  = -static -lippsmergedem64t -lippcoreem64t -lippacemergedem64t -lippccemergedem64t -lippchemergedem64t -lippcvemergedem64t \
         -lippdcemergedem64t -lippdiemergedem64t -lippgenemergedem64t -lippiemergedem64t -lippjemergedem64t -lippmemergedem64t \
         -lippremergedem64t -lippscemergedem64t -lippsemergedem64t -lippsremergedem64t -lippvcemergedem64t -lippvmemergedem64t
   else
      intel_LDINCL  =  -Wl,-Bdynamic -lguide -lipps -lippcore -lX11 -lm -pthread
   endif
endif

mk5access_LDFLAGS = -lmark5access
ifneq ($(FLAG_OSX), true)
   mk5access_LDFLAGS := -Wl,-Bstatic $(mk5access_LDFLAGS) -Wl,-Bdynamic
endif

ifeq ($(FLAG_HAVE_PLPLOT), true)
   ifeq ($(FLAG_OSX), true)
      plplot_LDFLAGS = -lplplotcxx
   else
      plplot_LDFLAGS = -lplplot
   endif
else
   plplot_LDFLAGS =
endif

all: swspectrometer

clean: 
	rm -f $(BASEOBJS) swspectrometer

swspectrometer: $(BASEOBJS) $(BUILD_NUMBER_FILE)
	$(CC) -g -O3 $(BASEOBJS) $(intel_LDFLAGS) $(mk5access_LDFLAGS) $(plplot_LDFLAGS) $(intel_LDINCL) $(BUILD_NUMBER_LDFLAGS) -o swspectrometer

.cpp.o:
	$(CC) $(intel_CFLAGS) -c $< -o $@

install: swspectrometer
	cp swspectrometer /usr/local/bin

# ##### BUILD NUMBER
ifneq ($(FLAG_OSX), true)
   include Buildnumber.inc
endif
